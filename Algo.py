class Permutation:
    def __init__(self, order, desc):
        self.order = order
        self.desc = desc

    def __repr__(self):
        return self.cyclic_form()

    def __eq__(self,other):
        if isinstance(other, Permutation):
            return (self.order == other.order and self.desc == other.desc)
        return NotImplemented

    def inverse(self):
        desc = {}
        for i in self.desc:
            desc[self.desc[i]] = i
        return Permutation(self.order, desc)

    def cyclic_form(self):
        a = [0 for i in range(self.order)]
        ret = ""
        while(True):
            for i in range(self.order):
                if(a[i] == 0):
                    ret += "("
                    cur = i
                    while(True):
                        if(a[cur] == 1):
                            break
                        a[cur] = 1
                        ret += str(cur)
                        cur = self.desc[cur]
                    ret += ")"
                    break
                if(a[i] == 1 and i == self.order - 1):
                    return ret

def id(order):
    return Permutation(order,dict([(i,i) for i in range(order)]))

def compose(external, internal):
    perm = Permutation(internal.order,{})
    for i in external.desc:
        perm.desc[i] = external.desc[internal.desc[i]]
    return perm

def affect(perm, element):
    return perm.desc[element]

class PermGroup:
    def __init__(self, order, perms, name):
        self.order = order
        self.perms = perms
        self.name = name

def orbit(group, element):
    print("Calculating orbit of element {} in group {}".format(element,group.name))
    used = [-1 for i in range(group.order)]
    o = []
    u = []
    used[element] = 0
    o.append(element)
    u.append(id(group.order))
    print("Element {} is in the orbit. Permutation associated: {}".format(element,u[o.index(element)]))
    while(True):
        for i in range(len(used)):
            if(used[i] == 0):
                for perm in group.perms:
                    a = affect(perm,i)
                    if(used[a] == -1):
                        used[a] = 0
                        o.append(a)
                        u.append(compose(perm, u[o.index(i)]))
                        print("Element {} is in the orbit. Permutation associated: {}".format(a,
                                                                                              u[o.index(a)]))
                    a = affect(perm.inverse(),i)
                    if (used[a] == -1):
                        used[a] = 0
                        o.append(a)
                        u.append(compose(perm.inverse(), u[o.index(i)]))
                        print("Element {} is in the orbit. Permutation associated: {}".format(a,
                                                                                              u[o.index(a)]))
                used[i] = 1
                break
            if(used[i] != 0 and i == len(used)-1):
                print("Orbit calculation complete. Orbit order: {}".format(len(o)))
                print()
                return (o,u)

def phi(generator_elem, u_elem, element, o, u):
    effect = affect(compose(generator_elem,u_elem),element)
    return u[o.index(effect)]

def table_entry(perm, order):
    for i in range(order):
        if affect(perm,i) == i:
            continue
        else:
            return (i,affect(perm,i))

def sims_filter(generator_list,n, order):
    print("Optimizing stabilizator generetor set using Sims filter")
    table = [[0 for j in range(n)] for i in range(n)]
    filtered_list = []
    for perm in generator_list:
        while(True):
            print("Current permutation: {}".format(perm))
            if perm == id(order):
                print("Identity permutation - Dropping")
                break
            if(table[table_entry(perm,order)[0]][table_entry(perm,order)[1]] == 0):
                print("(i,j) index for current permutation is empty. Filling in the current permutation.")
                table[table_entry(perm, order)[0]][table_entry(perm, order)[1]] = perm
                filtered_list.append(perm)
                break
            else:
                print("(i,j) index for current permutation is filled. Composing filled permutation and the inverse of current permutation")
                perm = compose(perm.inverse(),table[table_entry(perm, order)[0]][table_entry(perm, order)[1]])
                print("Resulting permutation: {}. Considering resulting permutation".format(perm))
                continue
    print("Sims filtering complete. Order of the resulting set: {}".format(len(filtered_list)))
    print()
    return filtered_list


def check_membership(perm, basicOrbits):
    print("Testing permutation {} for group membership".format(perm))
    counter = 0
    while perm != id(perm.order):
        print("Checking for orbit {}".format(counter))
        if counter == len(basicOrbits):
            print("The permutation is not identity in trivial group")
            return False
        effect = affect(perm,counter)
        print("Effect of current permutation on element {} : {}".format(counter,effect))
        if not effect in basicOrbits[counter][0]:
            print("Effect not in orbit")
            return False
        else:
            perm = compose(basicOrbits[counter][1][basicOrbits[counter][0].index(effect)].inverse(),perm)
            print("Effect in orbit, produced permutation: {}".format(perm))
            counter += 1
    return True

G = PermGroup(6,[Permutation(6,{2:3,3:5,5:4,4:2,0:0,1:1}),Permutation(6,{1:5,5:2,2:3,3:4,4:1,0:0})],"Initial Group")

basicOrbitSizes = []
current_elem = 0
current_group = G
step_count = 0
basicOrbits = []

while(True):
    print("---------------------- Step {} ---------------------------------".format(step_count))
    step_count += 1
    orb = orbit(current_group, current_elem)
    basicOrbitSizes.append(len(orb[0]))
    basicOrbits.append(orb)
    stabilizer_generators = []
    print("Calculating generators for Stabilizer of {}".format(current_elem))
    for a in current_group.perms:
        for u in orb[1]:
            stabilizer_generators.append(compose(phi(a,u,current_elem,orb[0],orb[1]).inverse(),compose(a,u)))
            print("Permutation {} is in the stabilizer".format(compose(phi(a,u,current_elem,orb[0],orb[1]).inverse(),compose(a,u))))
    print("Stabilizer generator calculation complete. Number of stabilizer generators before Sims filter: {}".format(len(stabilizer_generators)))
    print()
    stabilizer_generators = sims_filter(stabilizer_generators, current_group.order, current_group.order)

    if not stabilizer_generators:
        print("Acquired the trivial group.")
        print("---------------------- Algorithm Terminates -------------------".format(step_count))
        break
    else:
        current_elem += 1
        current_group = PermGroup(G.order,stabilizer_generators,"Stabilizer of elements up to {}".format(str(current_elem-1)))
    print("---------------------- Step {} End -----------------------------".format(step_count-1))
    print()

print("Orbit sizes: {}".format(basicOrbitSizes))
GroupOrder = 1
for i in basicOrbitSizes:
    GroupOrder *= i

print("Initial group order: {}".format(GroupOrder))

sigma1 = Permutation(6,{1:4,4:1,2:3,3:5,5:2,0:0})
sigma2 = Permutation(6,{0:3,3:5,5:2,2:0,1:4,4:1})
sigma3 = Permutation(6,{0:2,2:0,1:4,4:3,3:5,5:1})

print("\nChecking whether given permutations are members of the group")
print("{} : {}\n".format(sigma1,check_membership(sigma1,basicOrbits)))
print("{} : {}\n".format(sigma2,check_membership(sigma2,basicOrbits)))
print("{} : {}\n".format(sigma3,check_membership(sigma3,basicOrbits)))
